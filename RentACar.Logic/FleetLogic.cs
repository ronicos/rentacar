﻿using RentACar.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using RentACar.Model.ViewModels;
using System.Web.Mvc;
using System.Web;

namespace RentACar.Logic
{
    public class FleetLogic : BaseLogic
    {
        public IEnumerable<Car> Search(SearchCarViewModel searchCarViewModel)
        {
            var query = DB.Fleet.Include(x => x.CarType).AsQueryable();

            query = query.Where(x => x.ValidForRent);

            if (!string.IsNullOrWhiteSpace(searchCarViewModel.Manufacturer))
            {
                query = query.Where(x => x.CarType.Manufacturer.Contains(searchCarViewModel.Manufacturer));
            }

            if (!string.IsNullOrWhiteSpace(searchCarViewModel.Model))
            {
                query = query.Where(x => x.CarType.Model.Contains(searchCarViewModel.Model));
            }

            if (!string.IsNullOrWhiteSpace(searchCarViewModel.Freetext))
            {
                query = query.Where(x => x.CarType.Model.Contains(searchCarViewModel.Freetext) ||
                     x.CarType.Manufacturer.Contains(searchCarViewModel.Freetext));
            }

            if (searchCarViewModel.ManualGear.HasValue)
            {
                query = query.Where(x => x.CarType.ManualGear == (searchCarViewModel.ManualGear));
            }

            if (searchCarViewModel.ManufactureYear.HasValue)
            {
                query = query.Where(x => x.CarType.ManufactureYear == (searchCarViewModel.ManufactureYear));
            }

            // search for a car which is available from a specified date
            if (searchCarViewModel.AvailableFrom.HasValue)
            {
                // query where rent alerady completed, or rent is future date
                query = query.Where(c => c.Rentals.Any(r => r.EndDate < searchCarViewModel.AvailableFrom ||
                    (r.StartDate > searchCarViewModel.AvailableFrom && r.StartDate > DateTime.Now)));
            }
            if (searchCarViewModel.AvailableUntil.HasValue)
            {
                // query where rent starts after our end date.
                query = query.Where(c => c.Rentals.Any(r => r.StartDate > searchCarViewModel.AvailableUntil));
            }

            return query.AsEnumerable();
        }

        public IEnumerable<Car> List()
        {
            return DB.Fleet.Include(f => f.CarType).ToList();
        }

        public void Create(Car car, int carTypeId, HttpPostedFileBase image)
        {
            car.ValidForRent = true;
            var fileName = "/Content/images/" + Guid.NewGuid().ToString() + image.FileName;
            car.Image = fileName;
            var rootPath = HttpContext.Current.Server.MapPath(fileName);
            image.SaveAs(rootPath);
            car.CarType = DB.CarTypes.First(c => c.Id == carTypeId);
            DB.Fleet.Add(car);
            DB.SaveChanges();
        }

        public Car Read(int id)
        {
            var car = DB.Fleet.Include(x => x.CarType).SingleOrDefault(x => x.Id == id);
            return car;
        }

        public void Update(Car car)
        {
            DB.Fleet.Attach(car);
            DB.Entry(car).State = System.Data.EntityState.Modified;
            DB.SaveChanges();
        }

        public void Delete(int id)
        {
            var Car = DB.Fleet.Find(id);
            DB.Entry(Car).State = System.Data.EntityState.Deleted;
            DB.SaveChanges();
        }

        public bool CarExists(int carNumber)
        {
            return DB.Fleet.Any(x => x.Id == carNumber);
        }

        public void ReturnCar(int carNumber)
        {
            var car = DB.Fleet.Find(carNumber);
            car.ValidForRent = true;
            DB.SaveChanges();
        }
    }
}
