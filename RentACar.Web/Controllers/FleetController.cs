﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RentACar.Model;
using RentACar.Model.ViewModels;
using RentACar.Logic;

namespace RentACar.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class FleetController : Controller
    {
        private FleetLogic fleet = new FleetLogic();
        private CarTypesLogic carTypes = new CarTypesLogic();

        [AllowAnonymous]
        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Search(SearchCarViewModel searchCarViewModel)
        {
            ViewBag.Results = fleet.Search(searchCarViewModel);

            return View();
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Return()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public ActionResult Return(ReturnCarViewModel carSearch)
        {
            if (!fleet.CarExists(carSearch.CarNumber))
            {
                ModelState.AddModelError("CarNumber", "Car does not exists");
                return View(carSearch);
            }

            fleet.ReturnCar(carSearch.CarNumber);
            return RedirectToAction("Message", "Home", new { message = "Thank you for returning the car." });
        }

        //
        // GET: /Fleet/

        public ActionResult Index()
        {
            return View(fleet.List());
        }

        //
        // GET: /Fleet/Details/5

        public ActionResult Details(int id = 0)
        {
            Car car = fleet.Read(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        //
        // GET: /Fleet/Create

        public ActionResult Create()
        {
            ViewBag.CarTypes = carTypes.GetSelectList();
            return View();
        }

        //
        // POST: /Fleet/Create

        [HttpPost]
        public ActionResult Create(Car car, int CarTypeId, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                fleet.Create(car, CarTypeId, image);
                return RedirectToAction("Index");
            }

            return View(car);
        }

        //
        // GET: /Fleet/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Car car = fleet.Read(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        //
        // POST: /Fleet/Edit/5

        [HttpPost]
        public ActionResult Edit(Car car)
        {
            if (ModelState.IsValid)
            {
                fleet.Update(car);
                return RedirectToAction("Index");
            }
            return View(car);
        }

        //
        // GET: /Fleet/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Car car = fleet.Read(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        //
        // POST: /Fleet/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            fleet.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            fleet.Dispose();
            base.Dispose(disposing);
        }
    }
}