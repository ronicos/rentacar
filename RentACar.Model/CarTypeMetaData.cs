﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RentACar.Model
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(CarTypeMetaData))]
    public partial class CarType
    {
    }

    internal sealed class CarTypeMetaData
    {
        [Range(2000, 2999)]
        public int ManufactureYear { get; set; }
    }
}
