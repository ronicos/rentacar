﻿using RentACar.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentACar.Logic
{
    public abstract class BaseLogic : IDisposable
    {
        internal RentACarEntities DB { get; set; }

        public BaseLogic()
        {
            DB = new RentACarEntities();
            DB.Configuration.ProxyCreationEnabled = false;
        }

        public void Dispose()
        {
            DB.Dispose();
        }
    }
}
