﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentACar.Model.ViewModels
{
    public class ReturnCarViewModel
    {
        [Required]
        public int CarNumber { get; set; }
    }
}
