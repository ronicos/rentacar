﻿using RentACar.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Web;

namespace RentACar.Logic
{
    public class OrdersLogic : BaseLogic
    {
        public IEnumerable<Order> List(int? id = null)
        {
            var query = DB.Orders.Include(o => o.CarNumber.CarType);

            if (id.HasValue)
            {
                query = query.Where(o => o.User.UserID == id);
            }

            return (query.ToList());
        }

        public Order Read(int id = 0)
        {
            var order = DB.Orders.Find(id);
            return order;
        }

        public void Create(Order order)
        {
            var car = DB.Fleet.FirstOrDefault(c => c.Id == order.CarNumber.Id);
            var user = DB.Users.FirstOrDefault(u => u.UserID == ((CustomIdentity)HttpContext.Current.User.Identity).UserId);
            order.CarNumber = car;
            order.User = user;
            DB.Orders.Add(order);
            DB.SaveChanges();
        }
    }
}
