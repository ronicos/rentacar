﻿using RentACar.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace RentACar.Logic
{
    public class CarTypesLogic : BaseLogic
    {
        public IEnumerable<CarType> List()
        {
            return DB.CarTypes.ToList();
        }

        public void Create(CarType carType)
        {
            DB.CarTypes.Add(carType);
            DB.SaveChanges();
        }

        public CarType Read(int id)
        {
            var carType = DB.CarTypes.Find(id);
            return carType;
        }

        public void Update(CarType carType)
        {
            DB.CarTypes.Attach(carType);
            DB.Entry(carType).State = System.Data.EntityState.Modified;
            DB.SaveChanges();
        }

        public void Delete(int id)
        {
            var carType = DB.CarTypes.Find(id);
            DB.Entry(carType).State = System.Data.EntityState.Deleted;
            DB.SaveChanges();
        }

        public SelectList GetSelectList()
        {
            return new SelectList(DB.CarTypes.AsEnumerable().Select(x => new { Id = x.Id, Text = x.Manufacturer + " " + x.Model + " - " + x.ManufactureYear.ToString() }), "Id", "Text");
        }
    }
}
