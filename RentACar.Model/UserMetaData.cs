﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RentACar.Model
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(UserMetadata))]
    public partial class User
    {
    }

    internal sealed class UserMetadata
    {
        [MinLength(4)]
        public string Username { get; set; }
        [MinLength(4)]
        public string Password { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "Email address is not valid")]
        public string Email { get; set; }
    }
}
