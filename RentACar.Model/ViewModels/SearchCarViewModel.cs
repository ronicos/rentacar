﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentACar.Model.ViewModels
{
    public class SearchCarViewModel
    {
        [Display(Name = "Manufacturer")]
        public string Manufacturer { get; set; }

        [Display(Name = "Manufacture Year")]
        public int? ManufactureYear { get; set; }

        [Display(Name = "Manual Gear")]
        public bool? ManualGear { get; set; }

        [Display(Name = "Model")]
        public string Model { get; set; }

        [Display(Name = "Available From")]
        public DateTime? AvailableFrom { get; set; }

        [Display(Name = "Available Until")]
        public DateTime? AvailableUntil { get; set; }

        [Display(Name = "Free text")]
        public string Freetext { get; set; }
    }
}
