﻿using Newtonsoft.Json;
using RentACar.Model;
using RentACar.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using System.Data.Entity;

namespace RentACar.Logic
{
    public class AccountLogic : BaseLogic
    {
        public void LogoutUser()
        {
            FormsAuthentication.SignOut();
        }

        public Dictionary<string, string> Login(LoginViewModel loginViewModel)
        {
            var errors = new Dictionary<string, string>();
            var user = DB.Users.Include(x => x.Roles).FirstOrDefault(x => x.Username == loginViewModel.UserName);

            if (user == null)
            {
                errors.Add("Username", "The username was not found");
            }
            else if (user.Password != loginViewModel.Password)
            {
                errors.Add("password", "Incorrect password");
            }
            else
            {
                LoginUser(loginViewModel.RememberMe, user);
            }

            return errors;
        }

        public Dictionary<string, string> Register(RegisterViewModel registerViewModel)
        {
            var errors = new Dictionary<string, string>();

            if (registerViewModel.BirthDate.AddYears(18) > DateTime.Now)
            {
                errors.Add("BirthDate", "Must be above 18");
            }

            if (DB.Users.Any(u => u.Username == registerViewModel.Username))
            {
                errors.Add("Username", "Username already exists");
            }

            if (!errors.Any())
            {
                var role = DB.Roles.FirstOrDefault(r => r.RoleName == "User");
                var user = new User()
                {
                    Email = registerViewModel.Email,
                    FirstName = registerViewModel.FirstName,
                    LastName = registerViewModel.LastName,
                    Password = registerViewModel.Password,
                    Roles = new List<Role>() { role },
                    UserIdNumber = registerViewModel.UserIdNumber,
                    Username = registerViewModel.Username,
                    BirthDate = registerViewModel.BirthDate
                };

                DB.Users.Add(user);
                DB.SaveChanges();
            }

            return errors;
        }

        public void LoginUser(bool RememberMe, User user)
        {
            var encryptedTicket = CreateUserTicket(RememberMe, user);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

            HttpContext.Current.Response.Cookies.Add(cookie);

            AuthenticateUser(cookie.Value);
        }

        public void TryLoadUserCredentials()
        {
            var cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (cookie == null)
            {
                return;
            }

            AuthenticateUser(cookie.Value);
        }

        private string CreateUserTicket(bool persistent, User user)
        {
            var ticket = new FormsAuthenticationTicket(
                version: 1,
                name: user.FirstName,
                issueDate: DateTime.Now,
                expiration: DateTime.Now.AddMinutes(HttpContext.Current.Session.Timeout),
                isPersistent: persistent,
                userData: JsonConvert.SerializeObject(user));

            var encryptedTicket = FormsAuthentication.Encrypt(ticket);
            return encryptedTicket;
        }

        private void AuthenticateUser(string ticket)
        {
            var decodedTicket = FormsAuthentication.Decrypt(ticket);
            var user = JsonConvert.DeserializeObject<User>(decodedTicket.UserData);
            var roles = user.Roles.Select(r => r.RoleName).ToArray();
            var identity = new CustomIdentity(decodedTicket.Name);
            identity.UserId = user.UserID;

            var principal = new GenericPrincipal(identity, roles);
            HttpContext.Current.User = principal;
        }
    }
}
