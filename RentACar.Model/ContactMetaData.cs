﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RentACar.Model
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(ContactMetadata))]
    public partial class Contact
    {
    }

    internal sealed class ContactMetadata
    {
        [Required]
        public string Message { get; set; }
    }
}
