
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 06/08/2014 00:02:52
-- Generated from EDMX file: D:\Projects\Students\GuyPla\RentACar\RentACar.Model\RentACarModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [RentACar];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_UserVsRoles_Roles]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserVsRoles] DROP CONSTRAINT [FK_UserVsRoles_Roles];
GO
IF OBJECT_ID(N'[dbo].[FK_UserVsRoles_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserVsRoles] DROP CONSTRAINT [FK_UserVsRoles_Users];
GO
IF OBJECT_ID(N'[dbo].[FK_CarTypeFleet]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Fleet] DROP CONSTRAINT [FK_CarTypeFleet];
GO
IF OBJECT_ID(N'[dbo].[FK_UserRental]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_UserRental];
GO
IF OBJECT_ID(N'[dbo].[FK_CarRental]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_CarRental];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Roles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Roles];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[CarTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CarTypes];
GO
IF OBJECT_ID(N'[dbo].[Fleet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Fleet];
GO
IF OBJECT_ID(N'[dbo].[Orders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Orders];
GO
IF OBJECT_ID(N'[dbo].[Contacts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Contacts];
GO
IF OBJECT_ID(N'[dbo].[UserVsRoles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserVsRoles];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Roles'
CREATE TABLE [dbo].[Roles] (
    [RoleID] int IDENTITY(1,1) NOT NULL,
    [RoleName] varchar(20)  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [UserID] int IDENTITY(1,1) NOT NULL,
    [Username] nvarchar(50)  NOT NULL,
    [Password] nvarchar(50)  NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [UserIdNumber] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NULL,
    [BirthDate] datetime  NULL
);
GO

-- Creating table 'CarTypes'
CREATE TABLE [dbo].[CarTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Manufacturer] nvarchar(max)  NOT NULL,
    [Model] nvarchar(max)  NOT NULL,
    [ManufactureYear] int  NOT NULL,
    [ManualGear] bit  NOT NULL,
    [DailyCost] int  NOT NULL,
    [OverdueCost] int  NOT NULL
);
GO

-- Creating table 'Fleet'
CREATE TABLE [dbo].[Fleet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Travelled] int  NOT NULL,
    [Image] nvarchar(max)  NOT NULL,
    [ValidForRent] bit  NOT NULL,
    [CarType_Id] int  NOT NULL
);
GO

-- Creating table 'Orders'
CREATE TABLE [dbo].[Orders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [StartDate] datetime  NULL,
    [EndDate] datetime  NULL,
    [ActualReturningDate] datetime  NULL,
    [User_UserID] int  NOT NULL,
    [CarNumber_Id] int  NOT NULL
);
GO

-- Creating table 'Contacts'
CREATE TABLE [dbo].[Contacts] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Email] nvarchar(max)  NULL,
    [Message] nvarchar(max)  NOT NULL,
    [Date] datetime  NULL
);
GO

-- Creating table 'UserVsRoles'
CREATE TABLE [dbo].[UserVsRoles] (
    [Roles_RoleID] int  NOT NULL,
    [Users_UserID] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [RoleID] in table 'Roles'
ALTER TABLE [dbo].[Roles]
ADD CONSTRAINT [PK_Roles]
    PRIMARY KEY CLUSTERED ([RoleID] ASC);
GO

-- Creating primary key on [UserID] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([UserID] ASC);
GO

-- Creating primary key on [Id] in table 'CarTypes'
ALTER TABLE [dbo].[CarTypes]
ADD CONSTRAINT [PK_CarTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Fleet'
ALTER TABLE [dbo].[Fleet]
ADD CONSTRAINT [PK_Fleet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [PK_Orders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Contacts'
ALTER TABLE [dbo].[Contacts]
ADD CONSTRAINT [PK_Contacts]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Roles_RoleID], [Users_UserID] in table 'UserVsRoles'
ALTER TABLE [dbo].[UserVsRoles]
ADD CONSTRAINT [PK_UserVsRoles]
    PRIMARY KEY NONCLUSTERED ([Roles_RoleID], [Users_UserID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Roles_RoleID] in table 'UserVsRoles'
ALTER TABLE [dbo].[UserVsRoles]
ADD CONSTRAINT [FK_UserVsRoles_Roles]
    FOREIGN KEY ([Roles_RoleID])
    REFERENCES [dbo].[Roles]
        ([RoleID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Users_UserID] in table 'UserVsRoles'
ALTER TABLE [dbo].[UserVsRoles]
ADD CONSTRAINT [FK_UserVsRoles_Users]
    FOREIGN KEY ([Users_UserID])
    REFERENCES [dbo].[Users]
        ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UserVsRoles_Users'
CREATE INDEX [IX_FK_UserVsRoles_Users]
ON [dbo].[UserVsRoles]
    ([Users_UserID]);
GO

-- Creating foreign key on [CarType_Id] in table 'Fleet'
ALTER TABLE [dbo].[Fleet]
ADD CONSTRAINT [FK_CarTypeFleet]
    FOREIGN KEY ([CarType_Id])
    REFERENCES [dbo].[CarTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CarTypeFleet'
CREATE INDEX [IX_FK_CarTypeFleet]
ON [dbo].[Fleet]
    ([CarType_Id]);
GO

-- Creating foreign key on [User_UserID] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_UserRental]
    FOREIGN KEY ([User_UserID])
    REFERENCES [dbo].[Users]
        ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UserRental'
CREATE INDEX [IX_FK_UserRental]
ON [dbo].[Orders]
    ([User_UserID]);
GO

-- Creating foreign key on [CarNumber_Id] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_CarRental]
    FOREIGN KEY ([CarNumber_Id])
    REFERENCES [dbo].[Fleet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CarRental'
CREATE INDEX [IX_FK_CarRental]
ON [dbo].[Orders]
    ([CarNumber_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------