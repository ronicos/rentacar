﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RentACar.Model;
using RentACar.Logic;

namespace RentACar.Web.Controllers
{
    [Authorize(Roles="Admin")]
    public class CarTypesController : Controller
    {
        private CarTypesLogic db = new CarTypesLogic();

        //
        // GET: /CarTypes/

        public ActionResult Index()
        {
            return View(db.List());
        }

        //
        // GET: /CarTypes/Details/5

        public ActionResult Details(int id = 0)
        {
            CarType cartype = db.Read(id);
            if (cartype == null)
            {
                return HttpNotFound();
            }
            return View(cartype);
        }

        //
        // GET: /CarTypes/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CarTypes/Create

        [HttpPost]
        public ActionResult Create(CarType cartype)
        {
            if (ModelState.IsValid)
            {
                db.Create(cartype);
                return RedirectToAction("Index");
            }

            return View(cartype);
        }

        //
        // GET: /CarTypes/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CarType cartype = db.Read(id);
            if (cartype == null)
            {
                return HttpNotFound();
            }
            return View(cartype);
        }

        //
        // POST: /CarTypes/Edit/5

        [HttpPost]
        public ActionResult Edit(CarType cartype)
        {
            if (ModelState.IsValid)
            {
                db.Update(cartype);
                return RedirectToAction("Index");
            }
            return View(cartype);
        }

        //
        // GET: /CarTypes/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CarType cartype = db.Read(id);
            if (cartype == null)
            {
                return HttpNotFound();
            }
            return View(cartype);
        }

        //
        // POST: /CarTypes/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            db.Delete(id);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}