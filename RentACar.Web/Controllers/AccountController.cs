﻿using Newtonsoft.Json;
using RentACar.Logic;
using RentACar.Model;
using RentACar.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RentACar.Web.Controllers
{
    public class AccountController : Controller
    {
        AccountLogic db = new AccountLogic();

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Logout()
        {
            db.LogoutUser();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel login)
        {
            var errors = db.Login(login);

            if (errors.Any())
            {
                foreach (var error in errors)
                {
                    ModelState.AddModelError(error.Key, error.Value);
                }

                return View(login);
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel registerViewModel)
        {
            if (ModelState.IsValid)
            {
                var errors = db.Register(registerViewModel);

                if (errors.Any())
                {
                    foreach (var error in errors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    }

                    return View(registerViewModel);
                }

                var login = new LoginViewModel()
                {
                    UserName = registerViewModel.Username,
                    Password = registerViewModel.Password,
                };

                Login(login);
                return RedirectToAction("Index", "Home");
            }

            return View(registerViewModel);
        }
    }
}
