﻿using Newtonsoft.Json;
using RentACar.Model;
using RentACar.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using System.Data.Entity;

namespace RentACar.Logic
{
    public class UserLogic
    {
        RentACarEntities db = new RentACarEntities();

        public Dictionary<string, string> Login(LoginViewModel loginViewModel)
        {
            var errors = new Dictionary<string, string>();
            db.Configuration.ProxyCreationEnabled = false;
            var user = db.Users.Include(x => x.Roles).FirstOrDefault();

            if (user == null)
            {
                errors.Add("Username", "The username was not found");
            }
            else if (user.Password != loginViewModel.Password)
            {
                errors.Add("password", "Incorrect password");
            }
            else
            {
                AccountLogic accountLogic = new AccountLogic();
                accountLogic.LoginUser(loginViewModel.RememberMe, user);
            }

            return errors;
        }

        public Dictionary<string, string> Register(RegisterViewModel registerViewModel)
        {
            var errors = new Dictionary<string, string>();

            if (registerViewModel.BirthDate.AddYears(18) > DateTime.Now)
            {
                errors.Add("BirthDate", "Must be above 18");
            }

            if (db.Users.Any(u => u.Username == registerViewModel.Username))
            {
                errors.Add("Username", "Username already exists");
            }

            if (!errors.Any())
            {
                var role = db.Roles.FirstOrDefault(r => r.RoleName == "User");
                var user = new User()
                {
                    Email = registerViewModel.Email,
                    FirstName = registerViewModel.FirstName,
                    LastName = registerViewModel.LastName,
                    Password = registerViewModel.Password,
                    Roles = new List<Role>() { role },
                    UserIdNumber = registerViewModel.UserIdNumber,
                    Username = registerViewModel.Username,
                    BirthDate = registerViewModel.BirthDate
                };

                db.Users.Add(user);
                db.SaveChanges();
            }

            return errors;
        }
    }
}
