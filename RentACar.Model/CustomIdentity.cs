﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace RentACar.Model
{
    public class CustomIdentity : GenericIdentity
    {
        public int UserId { get; set; }
        public CustomIdentity(string name)
            : base(name, "Forms")
        {

        }
    }
}
