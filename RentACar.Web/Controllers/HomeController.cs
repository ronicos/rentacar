﻿using RentACar.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RentACar.Web.Controllers
{
    public class HomeController : Controller
    {
        RentACarEntities db = new RentACarEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(Contact contact)
        {
            db.Contacts.Add(contact);
            db.SaveChanges();

            return RedirectToAction("Message", "Home", new { message = "Your message was saved and you will be reply shortly." });
        }

        public ActionResult Message(string message)
        {
            return View((object)message);
        }

        [Authorize(Roles="Admin")]
        public ActionResult Admin()
        {
            return View();
        }
    }
}
