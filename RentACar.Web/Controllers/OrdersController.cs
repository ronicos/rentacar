﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RentACar.Model;
using RentACar.Logic;

namespace RentACar.Web.Controllers
{
    [Authorize(Roles = "User")]
    public class OrdersController : Controller
    {
        private OrdersLogic ordersLogic = new OrdersLogic();
        private FleetLogic fleetLogic = new FleetLogic();

        //
        // GET: /Orders/

       [Authorize(Roles = "Admin")] 
        public ActionResult All()
        {
            var orders = ordersLogic.List();

            return View("Index", orders);
        }

        public ActionResult Index(int? id = null)
        {
            IEnumerable<Order> orders = null;

            if (id.HasValue)
            {
                orders = ordersLogic.List(id);
            }

            return View(orders);
        }

        //
        // GET: /Orders/Details/5

        public ActionResult Details(int id = 0)
        {
            Order order = ordersLogic.Read(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        //
        // GET: /Orders/Create

        public ActionResult Create(int id)
        {
            var car = fleetLogic.Read(id);

            var order = new Order()
            {
                CarNumber = car
            };

            return View(order);
        }

        //
        // POST: /Orders/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Order order)
        {
            if (ModelState.IsValid)
            {
                ordersLogic.Create(order);
                return RedirectToAction("Index", new { id=((CustomIdentity)User.Identity).UserId });
            }

            return View(order);
        }

        protected override void Dispose(bool disposing)
        {
            ordersLogic.Dispose();
            base.Dispose(disposing);
        }
    }
}